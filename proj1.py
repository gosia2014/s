# Projekt 1
# Sudoku

from time import time

class Timer:
    def __init__(t):
        t.Elapsed  = 0.0
        t.Starttime = time()
        t.Started = False

    def start(t):
        t.Starttime = time()
        t.Started = True

    def stop(t):
        t.Elapsed += (time() - t.Starttime)
        t.Started = False

    def getElapsed(t):
        if t.Started:
            return (t.Elapsed + time() - t.Starttime)
        else:
            return t.Elapsed

# ---- Start timing, the main part of the program ----

timer = Timer()
timer.start()

# -------------- Loading libraries -------------------

import numpy as np
import timeit

# -------------- Methods --------------

def checkRows(board):
    for i in range(0, 9):
        for j in range(1, 10):
            b = list(board[i].ravel())
            if b.count(j) > 1:
                return False
    return True

def checkCols(board):
    for i in range(0, 9):
        for j in range(1, 10):
            b = list(board[:, i].ravel())
            if b.count(j) > 1:
                return False
    return True

def checkSquares(board):
    for i in range(0, 9, 3):
        for j in range(0, 9, 3):
            b = list(board[i:i+3, j:j+3].ravel())
            for k in range(1, 10):
                if b.count(k) > 1:
                    return False
    return True

def checkBoard(board):
    return np.all([checkRows(board), checkCols(board), checkSquares(board)])

def checkFinish(board):
    return board.min() > 0

def solve(board):
    print str(board).replace('0', '?')
    if not checkBoard(board):
        return False
    if checkFinish(board):
        print
        print "Solved Sudoku: "
        print board
        return True
    i, j = np.unravel_index(board.argmin(), board.shape)
    for values in range(1, 10):
        board[i, j] = values
        if solve(board):
            return True
    board[i, j] = 0
    return False

# -------------------------------------
# initialization of the array, the input data
board = [
    [1,4,0, 0,6,0, 0,0,0],
    [7,0,0, 1,5,4, 0,9,6],
    [0,3,6, 0,0,0, 0,4,0],
    [2,0,0, 0,7,0, 0,0,9],
    [8,0,0, 2,0,1, 0,0,7],
    [6,0,0, 0,9,0, 0,0,4],
    [0,8,0, 0,0,0, 4,2,0],
    [0,0,0, 4,8,9, 0,6,5],
    [0,0,0, 0,1,0, 0,8,3]]

board = np.array(board)

# -------- Print results --------
print solve(board)
timer.stop()
print "Time to solve Sudoku:   " + str(timer.getElapsed()) + " seconds"