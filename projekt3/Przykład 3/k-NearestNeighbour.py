# zrodlo: http://insideourminds.net/python-simple-k-nearest-neighbours-classifier/
import numpy as np
import scipy.spatial.distance as ssd
import time

def read_data(fn):
    """ czyta zestaw danych i rozdziela je na
        dane wejsciowe oraz etykiety danych
    """

    # czyta dane z pliku
    with open(fn) as f:
         raw_data = np.loadtxt(f, delimiter= ',', dtype="float",
             skiprows=1, usecols=None)
    # inicializuje liste
    data = []; label = []
    # przypisuje dane wejsciowe i naglowki
    for row in raw_data:
        data.append(row[:-1])
        label.append(int(row[-1]))
    # zwraca dane w tablicy
    return np.array(data), np.array(label)

def knn(k, dtrain, dtest, dtr_label, dist=1):
    """ k-nearest neighbors """
    # inicializowanie listy
    pred_class = []
    # w kazdym przypadku danych testowych oblicza odleglosc
    # w odniesieniu do danych treningowych

    for ii, di in enumerate(dtest):
        distances = []  # inicializacja listy odleglosci
        for ij, dj in enumerate(dtrain):
            # obliczanie odleglosci
            distances.append((calc_dist(di,dj,dist), ij))
        # k-sasiadow
        k_nn = sorted(distances)[:k]

        pred_class.append(classify(k_nn, dtr_label))

    # zwracanie przewidywanej klasy
    return pred_class

def calc_dist(di,dj,i=1):
    """ przelicza odleglosc dla obecnie
     dzialajacej funkcji odleglosciowej"""
    if i == 1:
        return ssd.euclidean(di,dj) # funkcja Euclidean
    elif i == 2:
        return ssd.cityblock(di,dj) # funkcja Manhattan
    elif i == 3:
        return ssd.cosine(di,dj)    # funkcja Cosine

def classify(k_nn, dtr_label):
    """ pobiera, przechowuje i klasyfikuje etykiety klas"""

    dlabel = []
    for dist, idx in k_nn:
        # pobieranie i przechowuje etykiety klas
        dlabel.append(dtr_label[idx])

    # zwraca przewidywana klase
    return np.argmax(np.bincount(dlabel))

def evaluate(result):
    """ tworzy tablice wynikow do przechowywania  oceny
        czy dana klasyfikacja jest prawidlowa"""

    # tworzy tablice
    eval_result = np.zeros(2,int)
    for x in result:
        # inkrementuje poprawne przewidywanie
        if x == 0:
            eval_result[0] += 1
        # inkrementuje bledne przewidywanie
        else:
            eval_result[1] += 1
    # zwraca rezultaty
    return eval_result

def main():
    """ klasyfikator k-nearest neighbors  """

    # inicjalizacja czasu
    start = time.clock()

    # dane testowe, 1 = test danych raka piersi,
    # 2 = test danych teczowki oka
    data_tests = [1,2]

    for d in data_tests:
        if d == 1:
            # czyta zbior danych raka piersi
            dtrain, dtr_label = read_data('breast-cancer-train.csv')
            dtest, true_class = read_data('breast-cancer-test.csv')
        else:
            # czyta zbior danych dotyczacych irysow
            dtrain, dtr_label = read_data('iris-train.csv')
            dtest, true_class = read_data('iris-test.csv')

        # inicjalizacja K
        K = [1,3,7,11]

        # funkcja odleglosciowa dla euclidean (1), manhattan (2),
        # i cosine (3)
        dist_fn = [1,2,3]

        if d == 1:
            print "k-NN classification results for breast cancer data set:"
        else:
            print "k-NN classification results for iris data set:"

        print
        print "    Number of correct / wrong classified test records"
        print "k  | Euclidean dist | Manhattan dist | Cosine dist"

        # uruchamia klasyfikator kNN dla podanego k i funkcji odleglosciowej
        for i in range(len(K)):
            # rezultaty klasyfikaji dla funkcji odleglosciowych
            results = []
            for j in range(len(dist_fn)):

                pred_class = knn(K[i], dtrain, dtest, dtr_label, dist_fn[j])
                # oceny przewidywanego rezultatu
                eval_result = evaluate(pred_class-true_class)
                # przypisywanie wyniku do ocenianego wyniku klasyfikacji
                results.append(eval_result[0])
                results.append(eval_result[1])

            # wypisywanie rezultatow na ekranie
            print K[i], " |     ", results[0], "/", results[1], \
                "    |    ", results[2], "/", results[3], \
                "     |    ", results[4], "/", results[5]
            results = []
        print

    # wypisywanie czasu
    run_time = time.clock() - start
    print "Runtime:", run_time

if __name__ == '__main__':
    main()

