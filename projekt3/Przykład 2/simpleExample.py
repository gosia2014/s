# Zrodlo: http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html#sklearn.neighbors.KNeighborsClassifier

# Zaimportowanie odpowiedniego modulu
from sklearn.neighbors import KNeighborsClassifier

# Zbior testowy. Cyfry 0-9 mozna rozumiec na przyklad
# jako jakies przedmioty o jednolitych kolorach
X = [[0], [1], [2], [3], [4], [5], [6], [7], [8], [9]]

# Nazwy klas (grup), do ktorych naleza wyzej opisywane
# przedmioty czyli w tym przypadku sa to kolory
# wyzej zadeklarowanych przedmiotow.
# Przedmioty 0, 1 i 2 sa koloru zielonego.
# Przedmiot 3 jest koloru niebieskiego itd.
y = ['zielony', 'zielony', 'zielony', 'niebieski', 'szary', 'szary', 'szary', 'granatowy', 'granatowy', 'granatowy']

# Utworzenie klasyfikatora z parametrem k = 3
neigh = KNeighborsClassifier(n_neighbors = 3)

# Uzupelnienie klasyfikatora o odpowiednie dane
neigh.fit(X, y)

# Zaklasyfikowanie obiektu 2.1 do odpowiedniej klasy (grupy)
# czyli okreslenie jakiego jest koloru.
print(neigh.predict(2.1))