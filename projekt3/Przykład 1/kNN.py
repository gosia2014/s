import cv2
import numpy as np
import matplotlib.pyplot as plt

# Zestaw funkcji zawierajacych warosci (x,y) 25 znanych/treningowych danych
zbiorTreningowy = np.random.randint(0,100,(25,2)).astype(np.float32)

# Nadanie etykiet kazdemu elementowi: albo czerwonej albo niebieskiej odpowiednio z numerami 0 i 1
etykiety = np.random.randint(0,2,(25,1)).astype(np.float32)

# Bierze tylko obiekty nalezace do czerwonej rodziny i rysuje je na wykresie w formie czerownych gwiazdek
czerwony = zbiorTreningowy[etykiety.ravel()==0]
plt.scatter(czerwony[:,0],czerwony[:,1],80,'r','*')

# Bierze tylko obiekty nalezace do niebieskiej rodziny i rysuje je na wykresie w formie niebieskich kwadratow
niebieski = zbiorTreningowy[etykiety.ravel()==1]
plt.scatter(niebieski[:,0],niebieski[:,1],80,'b','s')

# Bierze jeden nowy obiekt, ktory ma pozniej zostac zaklasyfikowany do odpowiedniej rodziny, rysuje go na wykresie w formie zielonego kolka
nowyObiekt = np.random.randint(0,100,(1,2)).astype(np.float32)
plt.scatter(nowyObiekt[:,0],nowyObiekt[:,1],80,'g','o')

k = 3 # podawana tutaj cyfra oznacza ilosc sasiadow k
knn = cv2.KNearest()
knn.train(zbiorTreningowy,etykiety)
ret, wyniki, sasiedzi ,odleglosc = knn.find_nearest(nowyObiekt, k)

print "wynik(klasa): ", wyniki,"\n"
print "sasiedzi(ich klasy): ", sasiedzi,"\n"
print "odleglosc: ", odleglosc

plt.show()
